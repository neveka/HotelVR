using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PathFinder
{
	public float uniformStep = 4.0f;
	public float minCorridorWidth = 2.0f;
	private float _waveDamping = 0.8f;
	private int _iterations = 100;
	private float _currentWaveValue = 1;
	private List<List<TipCube>> _corridors = new List<List<TipCube>>();
	public void StartWave(TipCube [][] tipCubes, StairwayGaps stairwayGaps)
	{
		//Debug.Log("StartWave "+stairwayGaps.list.Count);
		if(stairwayGaps.list.Count == 0)
			return;
		int idx = Random.Range(0, stairwayGaps.list.Count);
		Stairway gap = stairwayGaps.list[idx];
		for(int x=0; x<tipCubes.Length; x++)
		{
			for(int z=0; z<tipCubes[x].Length; z++)
			{		
				if(gap.bounds.Contains(tipCubes[x][z].bounds.center))
				{
					tipCubes[x][z].Value = _currentWaveValue;
					Vector3 dir = gap.transform.forward.normalized;
					if(x+(int)dir.x>=0 && x+(int)dir.x<tipCubes.Length && z+(int)dir.z>=0 && z+(int)dir.z<tipCubes[x].Length)
					{
						if(tipCubes[x+(int)dir.x][z+(int)dir.z].Value == 0)
							tipCubes[x+(int)dir.x][z+(int)dir.z].Value = _currentWaveValue*_waveDamping;
					}
					else
						Debug.Log("strange dir "+dir+" "+(x+(int)dir.x)+" "+(z+(int)dir.z)+" "+tipCubes.Length+" "+tipCubes[x].Length);
				}
				Stairway gapi = stairwayGaps.IsInGap(tipCubes[x][z].bounds.center);
				if(gapi && gap != gapi)
				{
					tipCubes[x][z].Value = -1;
					Vector3 dir = gapi.transform.forward.normalized;
					if(x+(int)dir.x>=0 && x+(int)dir.x<tipCubes.Length && z+(int)dir.z>=0 && z+(int)dir.z<tipCubes[x].Length)
					{	
						if(!stairwayGaps.IsInGap(tipCubes[x+(int)dir.x][z+(int)dir.z].bounds.center))
						{
							List<TipCube> corridor = new List<TipCube>();
							corridor.Add(tipCubes[x+(int)dir.x][z+(int)dir.z]);
							_corridors.Add(corridor);	
						}	
					}
					else
						Debug.Log("strange dir "+dir+" "+(x+(int)dir.x)+" "+(z+(int)dir.z)+" "+tipCubes.Length+" "+tipCubes[x].Length);					
				}				
			}
		}		
		
		_currentWaveValue *= _waveDamping;
		
		for(int i=0; i<_iterations; i++)
			ContinueWave(tipCubes);	
		
		for(int i=0; i<_iterations; i++)
			ContinueCorridors(tipCubes);
		
		foreach(List<TipCube> corridor in _corridors)
		{
			_currentWaveValue = -1*_waveDamping;
			foreach(TipCube cube in corridor)
			{
				cube.Value = _currentWaveValue;
				_currentWaveValue *= _waveDamping;
			}
		}
	}
	
	private void ContinueCorridors(TipCube [][] tipCubes)
	{
		foreach(List<TipCube> corridor in _corridors)
		{
			TipCube lastCube = corridor[corridor.Count-1];
			int x = lastCube.coordX;
			int z = lastCube.coordZ;
			TipCube hottest = lastCube;
			
			if(x>0 && lastCube.bounds.size.z>minCorridorWidth && CanBeHottest(tipCubes[x-1][z], hottest))
				hottest = tipCubes[x-1][z];
			if(x<tipCubes.Length-1 && lastCube.bounds.size.z>minCorridorWidth && CanBeHottest(tipCubes[x+1][z], hottest))
				hottest = tipCubes[x+1][z];
			if(z>0 && lastCube.bounds.size.x>minCorridorWidth && CanBeHottest(tipCubes[x][z-1], hottest))
				hottest = tipCubes[x][z-1];
			if(z<tipCubes[x].Length-1 && lastCube.bounds.size.x>minCorridorWidth && CanBeHottest(tipCubes[x][z+1], hottest))
				hottest = tipCubes[x][z+1];		
			if(hottest != corridor[corridor.Count-1])
				corridor.Add(hottest);
		}
		_currentWaveValue *= _waveDamping;
	}
	
	private bool CanBeHottest(TipCube newHottest, TipCube hottest)
	{
		return !hottest||(newHottest.Value > hottest.Value && newHottest.Value>0 && newHottest.Value<1);
	}
	
	private void ContinueWave(TipCube [][] tipCubes)
	{
		for(int x=0; x<tipCubes.Length; x++)
		{
			for(int z=0; z<tipCubes[x].Length; z++)
			{
				if(tipCubes[x][z].Value == 0)
				{
					if(x>0                    && tipCubes[x][z].bounds.size.z>minCorridorWidth && tipCubes[x-1][z].Value == _currentWaveValue)
						tipCubes[x][z].Value = _currentWaveValue*_waveDamping;
					if(x<tipCubes.Length-1    && tipCubes[x][z].bounds.size.z>minCorridorWidth && tipCubes[x+1][z].Value == _currentWaveValue)
						tipCubes[x][z].Value = _currentWaveValue*_waveDamping;
					if(z>0                    && tipCubes[x][z].bounds.size.x>minCorridorWidth && tipCubes[x][z-1].Value == _currentWaveValue)
						tipCubes[x][z].Value = _currentWaveValue*_waveDamping;
					if(z<tipCubes[x].Length-1 && tipCubes[x][z].bounds.size.x>minCorridorWidth && tipCubes[x][z+1].Value == _currentWaveValue)
						tipCubes[x][z].Value = _currentWaveValue*_waveDamping;					
				}
			}
		}
		_currentWaveValue *= _waveDamping;
	}	
	
}
