using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
	
[System.Serializable]
public class HouseGenerator: Generator
{
	public HouseGenerator(GeneratorInfo info):base(info) {}

	public void GenerateFloors(House house, FloorGenerator floorGenerator)
	{
		for(int i=0; i< _info.floorsCount; i++)
		{
			Floor newFloor = floorGenerator.GenerateFloor(i);
			newFloor.transform.parent = house.transform;	
			house.floors.Add(newFloor);
		}
	}
	
	public void GenerateStairwaysAndCorridors(House house, StairwayGenerator stairwayGenerator, FloorGenerator floorGenerator)
	{
		Debug.Log("GenerateStairwaysAndCorridors");
		//while(true)
		{
			/*yield return house.StartCoroutine(*/stairwayGenerator.FindStairwayBounds(house.floors)/*)*/;
			for(int i=0; i< stairwayGenerator.stairwayBounds.Count; i++)
			{
				Stairway stairsway = stairwayGenerator.GenerateStairway(i);
				stairsway.transform.parent = house.transform;
				for(int j=stairsway.startFloor; j<stairsway.finishFloor; j++)
					house.floors[j].AddStairwayGap(stairsway);				
				house.stairways.Add(stairsway);
			}	
			GenerateCorridors(house, floorGenerator);
			//break;
		}
	}		
	
	private void GenerateCorridors(House house, FloorGenerator floorGenerator)
	{
		for(int i=house.floors.Count-1; i>=0; i--)
		{
			floorGenerator.GenerateGorizontals(house.floors[i]);
			floorGenerator.FindCorridors(house.floors[i]);
		}		
	}	
}
