using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Generator
{
	protected readonly GeneratorInfo _info;

	public Generator(GeneratorInfo info)
	{
		_info = info;
	}	
}

[System.Serializable]
public class GeneratorInfo
{
	public Cube cubePrefab;
	public Cube wallPrefab;
	public TipCube tipCubePrefab;
	public Stairs stairsPrefab;	
	public Door doorPrefab;
	
	public float side = 50;
	public int floorsCount = 2;
	public float ceilingHeight = 4;	
}
