using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class House : MonoBehaviour 
{
	public List<Floor> floors = new List<Floor> ();
	public List<Stairway> stairways = new List<Stairway>(); 
	public GeneratorInfo info;

	void Awake () 
	{
		info.cubePrefab = (Resources.Load("MultiCube2") as GameObject).GetComponent<Cube>();
		info.wallPrefab = (Resources.Load("MultiCube1") as GameObject).GetComponent<Cube>();
		info.tipCubePrefab = (Resources.Load("TipCube") as GameObject).GetComponent<TipCube>();
		info.stairsPrefab = (Resources.Load("Stairs") as GameObject).GetComponent<Stairs>();		
		info.doorPrefab = (Resources.Load("Door") as GameObject).GetComponent<Door>();
		Debug.Log("Loaded "+info.cubePrefab+" "+info.tipCubePrefab+" "+info.stairsPrefab);
		
		HouseGenerator houseGenerator = new HouseGenerator(info);
		FloorGenerator floorGenerator = new FloorGenerator(info);
		StairwayGenerator stairwayGenerator = new StairwayGenerator(info);

		houseGenerator.GenerateFloors(this, floorGenerator);
		/*StartCoroutine(*/houseGenerator.GenerateStairwaysAndCorridors(this, stairwayGenerator, floorGenerator)/*)*/;
	}
}
