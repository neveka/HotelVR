﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class CombineMeshes : MonoBehaviour {

	// Use this for initialization
	void Awake () 
	{
		MeshFilter meshFilter = GetComponent<MeshFilter>();
		MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
		CombineInstance[] combine = new CombineInstance[meshFilters.Length];
		int i = 0;
		while (i < meshFilters.Length) {
			if (meshFilters [i] != meshFilter) {
				combine [i].mesh = meshFilters [i].sharedMesh;
				combine [i].transform = meshFilters [i].transform.localToWorldMatrix;
				Destroy (meshFilters [i].GetComponent<MeshRenderer> ());
			}
			i++;
		}

		meshFilter.mesh = new Mesh();
		meshFilter.mesh.CombineMeshes(combine);
	}
}
