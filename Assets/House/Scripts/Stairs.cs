using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Stairs : MonoBehaviour 
{
		public GameObject content;
		private List<BoxCollider> colliders;
		void Start () 
		{
				colliders =	new List<BoxCollider>(GetComponentsInChildren<BoxCollider> ());
				colliders.ForEach (c => c.isTrigger = true);
		}
	
		// Update is called once per frame
		void Update () {
				colliders.ForEach (c => c.isTrigger = false);
		}
}
