using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
	
[System.Serializable]
public class StairwayGenerator: Generator
{
	public List<Bounds> stairwayBounds = new List<Bounds>();
	public List<int> stairwayOrientations = new List<int>();	

	public StairwayGenerator(GeneratorInfo info):base(info) {}
	
	public Stairway GenerateStairway(int index)
	{
		Stairway stairsway = (new GameObject()).AddComponent<Stairway>();
		stairsway.name = "Stairway"+index;	
		stairsway.bounds = stairwayBounds[index];
		stairsway.transform.position = stairsway.bounds.center;
		stairsway.startFloor = (int)(stairsway.bounds.min.y/_info.ceilingHeight);
		stairsway.finishFloor = (int)(stairsway.bounds.max.y/_info.ceilingHeight);
		for(int i=0; i<stairsway.finishFloor-stairsway.startFloor; i++)
		{
			Stairs stairs = (Stairs)GameObject.Instantiate(_info.stairsPrefab);
			stairs.transform.parent = stairsway.transform;
			stairs.transform.localPosition = Vector3.up*(i*_info.ceilingHeight-(stairsway.bounds.max.y-stairsway.bounds.min.y)/2);
			if(i==stairsway.finishFloor-stairsway.startFloor-1)
				stairs.content.SetActive(false);
		}
		stairsway.transform.rotation = Quaternion.Euler(0, 90*stairwayOrientations[index], 0);
		return stairsway;
	}
	
	public void FindStairwayBounds(List<Floor> floors)
	{
		Bounds bounds = new Bounds(new Vector3(0,_info.floorsCount*_info.ceilingHeight/2,0), 
			new Vector3(_info.side, _info.floorsCount*_info.ceilingHeight, _info.side));//?
			
		int [] stairsOnFloor = new int[_info.floorsCount];
		while(MinStairsOnFloor(stairsOnFloor)<3)
		{	
			stairwayBounds.Clear();
			stairwayOrientations.Clear();
			stairwayBounds.Add(CreateBounds(0, 0, 0, 1, _info.ceilingHeight, 0));
			stairwayOrientations.Add(0);			
			for(int j=0; j<_info.floorsCount; j++)
				stairsOnFloor[j] = 0;
			stairsOnFloor[0] = 1;
			int counter = 0;
			while(counter++<10000)//?
			{
				int length = UnityEngine.Random.Range(2, floors.Count+1);
				int centerFloor = UnityEngine.Random.Range(length/2, floors.Count+1-length/2);
				int finishFloor = centerFloor+length/2;
				int startFloor = centerFloor-length/2;
				float centerX = UnityEngine.Random.Range(-_info.side/2+5, _info.side/2-5);
				float centerZ = UnityEngine.Random.Range(-_info.side/2+5, _info.side/2-5);
				int orientation = UnityEngine.Random.Range(0, 4);
				
				Bounds b = CreateBounds(centerX, centerZ, startFloor, finishFloor, _info.ceilingHeight, orientation);
				Bounds b2 = CreateAdditionalBounds(b, orientation);
				b2.Encapsulate(b);
				bool ok = bounds.Contains(b2.min) && bounds.Contains(b2.max);
				for(int j = 0; j<stairwayBounds.Count; j++)
					if(IsCollision(b2, stairwayBounds[j]))
				{
					ok = false;
					break;
				}
				if(ok)
				{
					for(int j=startFloor; j<finishFloor; j++)
						stairsOnFloor[j]++;	
		//TipCube tipCube = (TipCube)GameObject.Instantiate(_tipCubePrefab);
		//tipCube.InitHorizontal(floors[0].transform.parent, b2.min.x, b2.min.z, b2.max.x, b2.max.z);					
					stairwayOrientations.Add(orientation);
					stairwayBounds.Add(b);
				}	
			}
			Debug.Log("stairways on floor "+MinStairsOnFloor(stairsOnFloor));
			//yield return null;
		}	
		//yield break;
	}
	
	private int MinStairsOnFloor(int [] stairsOnFloor)
	{
		int min = 100;
		for(int j=0; j<_info.floorsCount; j++)
			if(stairsOnFloor[j]<min)
				min = stairsOnFloor[j];
		return min;
	}	
	
	private Bounds CreateBounds(float centerX, float centerZ, int startFloor, int finishFloor, float ceilingHeight, int orientation)
	{
		Vector3 center = new Vector3(centerX, (startFloor+finishFloor)*0.5f*ceilingHeight, centerZ);
		float xSize = 0;
		float zSise = 0;	
		if(orientation%2!=0)
		{
			xSize = 5;
			zSise = 4;
		}
		else
		{
			xSize = 4;
			zSise = 5;			
		}	
		return new Bounds(center, new Vector3(xSize, ceilingHeight*(finishFloor-startFloor), zSise));
	}
	
	private Bounds CreateAdditionalBounds(Bounds bounds, int orientation)
	{
		Vector3 dir = Quaternion.Euler(0, 90*orientation+90, 0)*(Vector3.left*4.5f);
		return new Bounds(bounds.center+dir, new Vector3(4, bounds.size.y, 4));
	}	
	
	private bool IsCollision(Bounds b, Bounds bounds)
	{
		float offset = 0;//?
		return (b.min.y-_info.ceilingHeight<bounds.max.y || b.max.y+_info.ceilingHeight>bounds.min.y) && 
			((b.min.x-offset<bounds.max.x && b.max.x+offset>bounds.max.x)||
			(b.min.z-offset<bounds.max.z && b.max.z+offset>bounds.max.z)||
			(b.min.x-offset<bounds.min.x && b.max.x+offset>bounds.min.x)||
			(b.min.z-offset<bounds.min.z && b.max.z+offset>bounds.min.z));
		//return bounds.Intersects(b);
	}	
}
