using UnityEngine;
using System.Collections;

public class TipCube : MonoBehaviour 
{	
	public Bounds bounds;
	public int coordX;
	public int coordZ;
	private float _thickness = 0.1f;
	private float _value = 0;
	
	public void InitHorizontal(Transform parentTransform, float fromX, float fromZ, float toX, float toZ)
	{
		transform.parent = parentTransform;
		float sizeX = toX-fromX;
		float sizeZ = toZ-fromZ;
		float centerX = (toX+fromX)/2;
		float centerZ = (toZ+fromZ)/2;
		transform.localScale = new Vector3(sizeX, _thickness, sizeZ);
		transform.localPosition = new Vector3(centerX, _thickness, centerZ);
		Value = 0;//Random.Range(0, 1f);
		bounds = new Bounds(transform.position, new Vector3(sizeX, _thickness, sizeZ));
	}
	
	public float Value
	{
		set{
			_value = value;
			if(_value>=0)
				GetComponent<Renderer>().material.SetColor("_Color", Color.Lerp(Color.red, Color.green, value));
			else
				GetComponent<Renderer>().material.SetColor("_Color", Color.Lerp(Color.white, Color.blue, -value));
		}
		get{ return _value; }
	}
}
