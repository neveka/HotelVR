using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class FloorGenerator: Generator
{
	public FloorGenerator(GeneratorInfo info):base(info){}

	public Floor GenerateFloor(int index)
	{
		Floor floor = (new GameObject()).AddComponent<Floor>();
		floor.transform.position = index*_info.ceilingHeight*Vector3.up;
		floor.name = "Floor"+index;
		float halfSide = _info.side/2;
		floor._xLines.Add(-halfSide);
		floor._xLines.Add(halfSide);
		floor._zLines.Add(-halfSide);
		floor._zLines.Add(halfSide);
		//halfSide += 0.5f;//?
		
		Cube newCube = (Cube)GameObject.Instantiate(_info.cubePrefab);
		newCube.InitVertical(floor.transform, -halfSide, -halfSide, halfSide, -halfSide, _info.ceilingHeight);
		
		newCube = (Cube)GameObject.Instantiate(_info.cubePrefab);
		newCube.InitVertical(floor.transform, -halfSide, -halfSide, -halfSide, halfSide, _info.ceilingHeight);
		
		newCube = (Cube)GameObject.Instantiate(_info.cubePrefab);
		newCube.InitVertical(floor.transform, halfSide, halfSide, halfSide, -halfSide, _info.ceilingHeight);
		
		newCube = (Cube)GameObject.Instantiate(_info.cubePrefab);
		newCube.InitVertical(floor.transform, halfSide, halfSide, -halfSide, halfSide, _info.ceilingHeight);		
		return floor;
	}
	
	public void GenerateGorizontals(Floor floor)
	{
		floor._xLines.Sort();
		floor._zLines.Sort();
		for(int x=0; x<floor._xLines.Count-1; x++)
			for(int z=0; z<floor._zLines.Count-1; z++)
			{
				Stairway gap1 = floor._stairwayGaps.IsInGap(new Vector3( (floor._xLines[x]+floor._xLines[x+1])/2, 
																		  floor.transform.position.y-2, 
				                                                         (floor._zLines[z]+floor._zLines[z+1])/2));
				Stairway gap2 = floor._stairwayGaps.IsInGap(new Vector3( (floor._xLines[x]+floor._xLines[x+1])/2, 
				                                                          floor.transform.position.y+2, 
				                                                         (floor._zLines[z]+floor._zLines[z+1])/2));
				if(gap1 && gap1==gap2)
					continue;
				Cube newCube = (Cube)GameObject.Instantiate(_info.cubePrefab);
				newCube.InitHorizontal(floor.transform, floor._xLines[x], floor._zLines[z], floor._xLines[x+1], floor._zLines[z+1]);			
			}	
	}	
	
	public void FindCorridors(Floor floor)
	{
		GameObject tips =  new GameObject();
		tips.name = "Tips";
		tips.transform.parent = floor.transform;
		tips.transform.localPosition = Vector3.zero;		
		
		PathFinder pathFinder = new PathFinder();//?
		List<float> xUniformLines = CreateUniformLines(floor._xLines, pathFinder.uniformStep);
		List<float> zUniformLines = CreateUniformLines(floor._zLines, pathFinder.uniformStep);
		TipCube [][] tipCubes = new TipCube[xUniformLines.Count-1][];
		for(int x=0; x<xUniformLines.Count-1; x++)
		{
			tipCubes[x] = new TipCube[zUniformLines.Count-1];
			for(int z=0; z<zUniformLines.Count-1; z++)
			{
				tipCubes[x][z] = (TipCube)GameObject.Instantiate(_info.tipCubePrefab);
				tipCubes[x][z].name = "tip"+x+"x"+z;
				tipCubes[x][z].coordX = x;
				tipCubes[x][z].coordZ = z;
				tipCubes[x][z].InitHorizontal(tips.transform, xUniformLines[x], zUniformLines[z], xUniformLines[x+1], zUniformLines[z+1]);
			}
		}		
		pathFinder.StartWave(tipCubes, floor._stairwayGaps);
		tips.SetActive(false);
		floor.tipCubes = tipCubes;
		//find rooms here
		GenerateVerticals(floor);
	}
	
	private void GenerateVerticals(Floor floor)
	{
		TipCube [][] tipCubes = floor.tipCubes;
		for(int x=0; x<tipCubes.Length; x++)
		{
			for(int z=0; z<tipCubes[x].Length; z++)
			{
				/*if(x==0)
				{
					CreateWall(floor, tipCubes[0][z], false, false, false, true);
					CreateWall(floor, tipCubes[tipCubes.Length-1][z], true, true, true, false);
				}				
				if(z==0)
				{
					CreateWall(floor, tipCubes[x][0], false, false, true, false);
					CreateWall(floor, tipCubes[x][tipCubes[x].Length-1], true, true, false, true);
				}*/
				float xz = tipCubes[x][z].Value;
				if(x<tipCubes.Length-1)
				{
					float x1z = tipCubes[x+1][z].Value;
					if(Mathf.Abs(xz)!=1 && Mathf.Abs(x1z)!=1 && (xz*x1z<0||(xz*x1z==0 && xz+x1z<0)))
						CreateWall(floor, tipCubes[x][z], WallPosition.RIGHT, xz<x1z);
				}
				if(z<tipCubes[x].Length-1)
				{
					float xz1 = tipCubes[x][z+1].Value;
				 	if(Mathf.Abs(xz)!=1 && Mathf.Abs(xz1)!=1 && (xz*xz1<0||(xz*xz1==0 && xz+xz1<0)))
						CreateWall(floor, tipCubes[x][z], WallPosition.FAR, xz<xz1);
				}	
			}
		}		
	}	
	
	enum WallPosition
	{
		FAR,
		NEAR,
		LEFT,
		RIGHT
	}
	
	private void CreateWall(Floor floor, TipCube tipCube, WallPosition wallposition, bool flip)
	{
		Vector3 fromPosition = new Vector3(
			wallposition == WallPosition.FAR||wallposition == WallPosition.LEFT?tipCube.bounds.min.x:tipCube.bounds.max.x, 
			_info.ceilingHeight/2, 
			wallposition == WallPosition.NEAR||wallposition == WallPosition.RIGHT?tipCube.bounds.min.z:tipCube.bounds.max.z);
		Vector3 toPosition = new Vector3(
			wallposition == WallPosition.NEAR||wallposition == WallPosition.LEFT?tipCube.bounds.min.x:tipCube.bounds.max.x,   
			_info.ceilingHeight/2,
			wallposition == WallPosition.NEAR||wallposition == WallPosition.LEFT?tipCube.bounds.min.z:tipCube.bounds.max.z);		
		
		bool screenParallel = wallposition == WallPosition.FAR||wallposition == WallPosition.NEAR;
		
		Transform tr = null;
		if((tipCube.bounds.max.x-tipCube.bounds.min.x == _info.ceilingHeight && screenParallel)||
			(tipCube.bounds.max.z-tipCube.bounds.min.z == _info.ceilingHeight && !screenParallel))
		{
			Door newDoor = (Door)GameObject.Instantiate(_info.doorPrefab);
			tr = newDoor.transform;
		}
		else
		{
			Cube newCube = (Cube)GameObject.Instantiate(_info.wallPrefab);
			newCube.TuneMeshSizes(new Vector3((fromPosition-toPosition).magnitude, _info.ceilingHeight, newCube.thickness), _info.ceilingHeight, false);
			tr = newCube.transform;
		}
		
		tr.parent = floor.transform;	
		tr.localRotation = Quaternion.AngleAxis((screenParallel?(flip?0:180):(flip?90:-90)), Vector3.up);
		tr.localPosition = (fromPosition+toPosition)/2;
	}
	
	private List<float> CreateUniformLines(List<float> lines, float uniformStep)
	{
		List<float> uniformLines = new List<float>(lines);
		for(int x=0; x<lines.Count-1; x++)
		{
			float count = Mathf.Abs(lines[x]-lines[x+1])/uniformStep;
			int idx = uniformLines.IndexOf(lines[x]);
			for(int i=1; i<count; i++)
			{
				uniformLines.Insert(idx+i, lines[x]+(lines[x+1]-lines[x])*i/count);
			}
		}		
		return uniformLines;
	}	
}
