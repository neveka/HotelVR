using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Cube : MonoBehaviour 
{
	public float thickness = 0.1f;	
	public void InitHorizontal(Transform parentTransform, float fromX, float fromZ, float toX, float toZ)
	{
		transform.parent = parentTransform;
		TuneMeshSizes(new Vector3(Mathf.Abs(toX-fromX), thickness, Mathf.Abs(toZ-fromZ)), 4f, true);
		transform.localPosition = new Vector3((toX+fromX)/2, 0, (toZ+fromZ)/2);		
	}	
	
	public void InitVertical(Transform parentTransform, float fromX, float fromZ, float toX, float toZ, float ceilingHeight)
	{
		transform.parent = parentTransform;
		Vector3 fromPosition = new Vector3(fromX, ceilingHeight/2, fromZ);
		Vector3 toPosition = new Vector3(toX, ceilingHeight/2, toZ);
		TuneMeshSizes(new Vector3((fromPosition-toPosition).magnitude, ceilingHeight, thickness), 4f, false);
		transform.localRotation = Quaternion.AngleAxis((fromX-toX!=0?180*Mathf.Atan((fromZ-toZ)/(fromX-toX))/Mathf.PI:90), Vector3.up);
		transform.localPosition = (fromPosition+toPosition)/2;	
	}		
	
	public void TuneMeshSizes(Vector3 sizes, float scale, bool horizontal)
	{
		List<Mesh> meshes = new List<Mesh>();
	
		if(GetComponent<MeshFilter>())
			meshes.Add(GetComponent<MeshFilter>().mesh);
		MeshFilter [] filters = GetComponentsInChildren<MeshFilter>();
		foreach(MeshFilter filter in filters)
			meshes.Add(filter.mesh);
		
		foreach(Mesh mesh in meshes)
		{
	        Vector3[] vertices = mesh.vertices;
			Vector2[] uvs = mesh.uv;
	        int i = 0;
	        while (i < vertices.Length) 
			{
				uvs[i].x = sizes.x*(vertices[i].x+0.5f)/scale;
				uvs[i].y = (horizontal?(sizes.z*(vertices[i].z+0.5f)):(sizes.y*(vertices[i].y+0.5f)))/scale;
	            vertices[i].x = sizes.x*vertices[i].x;
				vertices[i].y = sizes.y*vertices[i].y;
	            vertices[i].z = sizes.z*vertices[i].z;
				i++;
	        }
			i = 0;	
			mesh.uv = uvs;
	        mesh.vertices = vertices;
	        mesh.RecalculateBounds();		
		}
		((BoxCollider)GetComponent<Collider>()).size = sizes;		
	}
}
