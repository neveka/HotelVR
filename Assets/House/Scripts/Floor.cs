using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StairwayGaps
{
	public List<Stairway> list = new List<Stairway>();
	public Stairway IsInGap(Vector3 point)
	{
		for(int i=0; i<list.Count; i++)
		{
			if(list[i].bounds.Contains(point))
			{	
				return list[i];
			}
		}
		return null;
	}	
}

public class Floor : MonoBehaviour 
{
	public List<float> _xLines = new List<float>();
	public List<float> _zLines = new List<float>();
	public StairwayGaps _stairwayGaps = new StairwayGaps();
	
	public TipCube [][] tipCubes;
	
	public void AddStairwayGap(Stairway stairway)
	{
		_stairwayGaps.list.Add(stairway);
		_xLines.Add(stairway.bounds.min.x);
		_xLines.Add(stairway.bounds.max.x);
		_zLines.Add(stairway.bounds.min.z);
		_zLines.Add(stairway.bounds.max.z);		
	}
}
